local cc = require('cc').new(17)

local function has_duplicates(challenge)
  local list = challenge.list
  local tmp = {}
  local resp

  for i = 1, #list do
    if tmp[list[i]] then
      resp = true
      break
    else
      tmp[list[i]] = true
    end
  end

  return { token = resp or false }
end


for _ = 1, 100 do
  local resp = cc:solve(has_duplicates, true)
  print(resp)
end

print(string.format('Average time to find the solution: %fs', cc:runtime_avg()))