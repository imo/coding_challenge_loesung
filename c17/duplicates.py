import requests
import json
import time

challenge_url = "https://cc.the-morpheus.de/challenges/17/"
solution_url = "https://cc.the-morpheus.de/solutions/17/"


# real simple solution 0.00000023s
def has_duplicates1(li):
    se = set(li)

    if len(se) != len(li):
        return True

    return False


# 0.00000026
def has_duplicates2(ch):
    li = ch["list"]
    di = {}

    for i in li:
        if i in di:
            return True
        else:
            di[i] = True

    return False


def benchmark(runs):
    for i in range(runs):
        ch = requests.get(challenge_url).json()
        start = time.time()
        sol = has_duplicates2(ch)
        bench = (time.time() - start)
        resp = requests.post(solution_url, json.dumps({"token": sol}))
        print(resp.text)

    print("{:.8f}".format(bench / runs))


print(benchmark(100))
