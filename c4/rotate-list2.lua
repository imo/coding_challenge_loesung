#!/usr/bin/env lua
-- Direktes manipulieren der Table mit table utils
-- Ineffizient bei großen Tabellen, da das umordnen von Werten innerhalb der Tabelle
-- recht rechenintensiv ist.

package.path = package.path .. ';./lib/?.lua'

local CC = require('cc')
local time = require('chronos')
local time_tbl = {}
local c4 = CC.new(4)

local function get_time(ttbl)
  local t = 0
  for _, v in ipairs(ttbl) do
    t = t + v
  end
  return (t / #ttbl)
end

local function rotate(list, k)
  local list = list
  
  for i = 1, k % #list do
    table.insert(list, 1, table.remove(list, #list))
  end
  
  return { token = list }
end

for i = 1, 100 do
  local resp_get = c4:get()
  local start = time.nanotime()
  local rlist = rotate(resp_get.list, resp_get.k)
  
  table.insert(time_tbl, time.nanotime() - start)
  
  local resp_post = c4:post(rlist)
  print(resp_post)
end

print(string.format('Average time to find the solution: %fs', get_time(time_tbl)))