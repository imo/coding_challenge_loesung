#!/usr/bin/env lua
local CC = require('cc')

-- function which return the num as binary
local function to_binary(num)
  if num == 0 then return 0 end
  
  num = tonumber(num)
  local btbl = {}

  -- devide number by 2 and add remainder to table and
  -- set num to quotient for the next interation
  -- do this until num is equal 0
  while num ~= 0 do
    table.insert(btbl, num % 2)
    num = num // 2
  end

  -- concat the values from the table to string, then revert it
  -- and return it as table with token as key and the string as value
  return { token = string.reverse(table.concat(btbl)) }
end

local cc = CC.new(6, { nojson = true })
for _ = 1, 100 do
  local result = cc:solve(to_binary)

  local resp = cc:submit(result)
  print(resp)
end

print(string.format('Average time to find the solution: %fs', cc:runtime_avg()))