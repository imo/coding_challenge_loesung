#!/usr/bin/env lua

package.path = package.path .. ';./lib/?.lua'

local CC = require('cc')
local time = require('chronos')
local url = 'https://cc.the-morpheus.de/'
local time_tbl = {}

local c3 = CC.new(3, true) -- mit dem zweiten Argument wird angegeben das LuaSec statt requests genutzt werden soll

local function get_time(ttbl)
  local t = 0
  for _, v in ipairs(ttbl) do
    t = t + v
  end
  return (t / #ttbl)
end

local function get_kten_max(resp_get)
  local k = resp_get.k - 1
  local list = resp_get.list
  
  table.sort(list)

  return { token = list[#list - k] }
end

for i = 1, 100 do
  local resp_get = c3:get()
  local start = time.nanotime()
  local kten_max = get_kten_max(resp_get)

  table.insert(time_tbl, time.nanotime() - start)
  
  local resp_post = c3:post(kten_max)
  print(resp_post)
end
os.execute('sleep 2')
print(string.format('Average time to find the solution: %fs', get_time(time_tbl)))