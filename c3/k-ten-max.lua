#!/usr/bin/env lua

-- can't use requests, because it depends on cjson which has problems with big numbers
--local requests = require('requests')
local https = require('ssl.https')
local ltn12 = require('ltn12')
local json = require('JSON')
local time = require('chronos')
local url = 'https://cc.the-morpheus.de/'
local time_tbl = {}

local function get_time(ttbl)
  local t = 0
  for _, v in ipairs(ttbl) do
    t = t + v
  end
  return (t / #ttbl)
end

local function get_challenge(url)
  local r, c, _ = https.request(url)
  
  if c == 200 then return json:decode(r) end
end

local function get_kten_max(resp_get)
  local k = resp_get.k - 1
  local list = resp_get.list
  
  table.sort(list)

  return json:encode({ token = list[#list - k] })
end

for i = 1, 100 do
  local resp_get = get_challenge(url .. 'challenges/3/')
  local start = time.nanotime()
  local kten_max = get_kten_max(resp_get)

  table.insert(time_tbl, time.nanotime() - start)
  
  local resp_post = {}
  local _, c, _ = https.request({
      url = url .. 'solutions/3/',
      method = 'POST',
      headers = {
        ['Content-Length'] = #kten_max,
        ['Content-Type'] = 'application/json'
      },
      source = ltn12.source.string(kten_max),
      sink = ltn12.sink.table(resp_post)
    })
end
os.execute('sleep 2')
print(string.format('Average time to find the solution: %fs', get_time(time_tbl)))