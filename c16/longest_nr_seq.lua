local CC = require('cc')

local function longest_number_seq(challenge)
  local list = challenge.list
  local rlist = {}
  local result = 0

  for _, n in ipairs(list) do
    if not rlist[n] then rlist[n] = true end
  end

  for i = 1, #list do
    if not rlist[list[i] - 1] then
      local j = list[i]
      
      while rlist[j] do
        j = j + 1
      end
      
      result = math.max(result, j - list[i])
    end
  end

  return { token = result }
end

local cc = CC.new(16)
for _ = 1, 100 do
  local resp = cc:solve(longest_number_seq, true)
  print(resp)
end

print(string.format('Average time to find the solution: %fs', cc:runtime_avg()))