#!/usr/bin/env lua5.3
local CC = require('cc')

-- function which gets the indizes of the summands
local function get_indizes(ch)
  local temp = {}
  local dist = math.maxinteger  -- lua >= 5.3

  for k1, v1 in ipairs(ch.list) do
    for k2, v2 in ipairs(ch.list) do
      if v1 + v2 == ch.k then
        local d = math.abs(k1 - k2)

        if d < dist then
          temp = {}
          table.insert(temp, k1 - 1)
          table.insert(temp, k2 - 1)

          dist = d
        end
      end
    end
  end

  return temp
end

local cc = CC.new(9)
for _ = 1, 100 do
  local result = { token = cc:solve(get_indizes) }
  local resp = cc:submit(result)
  print(resp)
end

print(string.format('Average time to find the solution: %fs', cc:runtime_avg()))