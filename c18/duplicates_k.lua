local cc = require('cc').new(18)

local function has_duplicates(challenge)
  local k, list = challenge.k, challenge.list
  local resp = false

  table.sort(list)
  local last = list[1]

  for i = 2, #list do
    if list[i] <= last + k then
      resp = true
      break
    else
      last = list[i]
    end
  end

  return { token = resp }
end


for _ = 1, 100 do
  local resp = cc:solve(has_duplicates, true)
  print(resp)
end

print(string.format('Average time to find the solution: %fs', cc:runtime_avg()))
