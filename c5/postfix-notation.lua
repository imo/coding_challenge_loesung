#!/usr/bin/env lua
package.path = package.path .. ';./lib/?.lua'

local CC = require('cc')
local time = require('chronos')
local inspect = require('inspect')
local time_tbl = {}

-- initialize new coding challange object
local c5 = CC.new(5, true)

-- function which calculates the average time
local function get_time(ttbl)
  local t = 0
  for _, v in ipairs(ttbl) do
    t = t + v
  end
  return (t / #ttbl)
end

-- function which returns the result depending on the operator
local function calc_interim_result(n2, n1, op)
  if op == '+' then
    return n1 + n2
  elseif op == '-' then
    return n1 - n2
  elseif op == '*' then
    return n1 * n2
  elseif op == '/' then
    return n1 / n2
  end
end

-- function which resolves the postfix notation
local function calculate_result(task)
  local stack = {}
  local token

  -- for every number or operator
  for t in task:gmatch('([%d-+*/]+)') do
    -- if t is an operator and the stack has at least 2 values
    -- take the two last values from the stack and calculate
    -- the result by using the matching operator
    if t:match('[-+*/]') and #stack >= 2 then
      table.insert(stack, calc_interim_result(table.remove(stack),
                                              table.remove(stack), t))
    -- else just add the number to the stack
    print(inspect(stack))
    else
      table.insert(stack, tonumber(t))
    end
  end

  -- stack shouldn't have more then one value at this point
  if #stack == 1 then
    -- if value is a negative one use math.ceil to round
    if stack[1] < 0 then
      token = string.format('%.f', math.ceil(stack[1]))
    -- else math.floor
    else
      token = string.format('%.f', math.floor(stack[1]))
    end
    
    return { token = tonumber(token) }
  end
end

for _ = 1, 100 do
  local resp_get = c5:get(false)
  print(resp_get)
  local start = time.nanotime()
  local result = calculate_result(resp_get)

  table.insert(time_tbl, time.nanotime() - start)

  local resp_post = c5:post(result)
  print(resp_post)
end

print(string.format('Average time to find the solution: %fs', get_time(time_tbl)))
