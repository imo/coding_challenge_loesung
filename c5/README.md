# Explanation, why some challenges might return an error

For the following math task in postfix notion, the server return **Error**:


```
159 527 134 / - 680 / 612 - 252 - 980 - 153 635 * 226 - * 393 * 717 * 501 - 507 * 632 + 216 * 927 - 900 * 701 * 703 + 593 * 434 682 * * 487 / 573 - 377 * 562 / 132 + 943 / 925 - 213 / 650 * 971 / 347 + 446 - 995 / 843 * 958 + 807 * 57 / 935 + 646 -
```


At some point during the calculation, the interim results are to big and Lua write them in scientic notion to the table. This is the output during the calculation, where you can see what I mean:

```
{ 159, 3.9328358208955 }
{ 155.0671641791 }
{ 0.22803994732221 }
{ -611.77196005268 }
{ -863.77196005268 }
{ -1843.7719600527 }
{ -1843.7719600527, 97155 }
{ -1843.7719600527, 96929 }
{ -178714972.31595 }
{ -70234984120.167 }
{ -50358483614160.0 }
{ -50358483614661.0 }
{ -2.5531751192633e+16 }
{ -2.5531751192632e+16 }
{ -5.5148582576086e+18 }
{ -5.5148582576086e+18 }
{ -4.9633724318477e+21 }
{ -3.4793240747252e+24 }
{ -3.4793240747252e+24 }
{ -2.0632391763121e+27 }
{ -2.0632391763121e+27, 295988 }
{ -6.1069403731826e+32 }
{ -1.2539918630765e+30 }
{ -1.2539918630765e+30 }
{ -4.7275493237984e+32 }
{ -8.4120094729509e+29 }
{ -8.4120094729509e+29 }
{ -8.9204766415174e+26 }
{ -8.9204766415174e+26 }
{ -4.1880172025903e+24 }
{ -2.7222111816837e+27 }
{ -2.8035130604364e+24 }
{ -2.8035130604364e+24 }
{ -2.8035130604364e+24 }
{ -2.8176010657652e+21 }
{ -2.3752376984401e+24 }
{ -2.3752376984401e+24 }
{ -1.9168168226411e+27 }
{ -3.3628365309494e+25 }
{ -3.3628365309494e+25 }
{ -3.3628365309494e+25 }
```

I take the final result, round it down and format it using string.format to format it back to decimal notation, so the final result is:

```
{"token":-6203353267137675264}
```

For this result the server will return **Error**. I guess it's because, during the calculation in scientific notation the precision get lost or is limited, so the final result is incorrect.
