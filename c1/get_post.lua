local CC = require('cc')

local cc = CC.new(1)
for i = 1, 100 do
  local resp = cc:solve(function(ch) return { token = tostring(ch) } end, true)
  print(resp)
end

print(string.format('Average time to find the solution: %fs', cc:runtime_avg()))