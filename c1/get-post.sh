#!/bin/bash

SOCKET="/dev/tcp/84.200.109.239/5000"

function get_challenge() {
  exec 3<> ${SOCKET}
  printf "GET /challenges/1/ HTTP/1.1\n" >&3
  printf "Connection:close\n\n" >&3
  echo $(tail -n1 <&3)
}

function submit() {
  local json="{\"token\":\"${1}\"}"
  exec 3<> ${SOCKET}
  printf "POST /solutions/1/ HTTP/1.1\n" >&3
  printf "Host: 84.200.109.239:5000\n" >&3
  printf "Content-Type: application/json\n" >&3
  printf "Content-Length: ${#json}\n\n" >&3
  printf "${json}" >&3
  echo $(tail -n1 <&3)
}

echo `submit $(get_challenge)` | grep -o Success || echo "Fail"
