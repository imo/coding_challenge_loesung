#!/usr/bin/env lua
-- Much shorter than the LuaSocket version, but
-- a bit slower in the avarage

local requests = require('requests')
local url = 'https://cc.the-morpheus.de/'
local start = os.clock()

local resp_get = requests.get(url .. 'challenges/1/')

if resp_get.status_code == 200 then
  local resp_post = requests.post({
      url = url .. 'solutions/1/',
      data = { token = resp_get.text }
    })
  local elapsed = os.clock() - start
  if resp_post.status_code == 200 then
    print(string.format('Code: %d\nResponse: %s\nTime: %fms', resp_post.status_code, resp_post.text, elapsed * 1000))
  else
    print(string.format('POST went wrong!\nCode: %d\nResponse: %s', resp_post.status_code, resp_post.text))
  end
else
  print(string.format('GET went wrong!\nCode: %d\nResponse: %s', resp_get.status_code, resp_get.text))
end