local Resolver = require('dns.resolver')
local dns_server = {
  '8.8.8.8', -- google
  '85.214.20.141', -- digitalcourage
  '194.150.168.168', -- ccc
  '84.200.69.80', -- dns.watch
  '82.141.39.32' -- opennic (edv-froehlich)
}
local sites = {
  'blog.chr.istoph.de',
  'blog.kokolor.es',
  'google.de',
  'facebook.de',
  'github.com',
  'lua.org'
}

local function resolve(server, sites)
  local r = Resolver.new({ server }, 2)
  r:disableCache()
  
  for _, s in ipairs(sites) do
    local start = os.clock()
    local _, e = r:resolve(s, 'A')
    if not e then
      print(string.format('  %s: %fms', s, (os.clock() - start) * 1000))
      start = nil
    else
      print(string.format('  %s: %s', s, e))
    end
    
    if s == sites[#sites] then print('\n') end
  end
end

for _, s in ipairs(dns_server) do
  print(string.format('Stats for DNS Server %s:', s))
  resolve(s, sites)
end

  