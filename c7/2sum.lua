#!/usr/bin/env lua
local CC = require('cc')

-- function which gets the indizes of the summands
local function get_indizes(ch)
  local temp = {}
  local comp
  
  -- while we iterate over the list and inserting elements
  -- into the temp hash table, we also look back if the
  -- current element's complement already exists in the hash
  -- table
  -- the benefit is, we don't have to go over the full list
  for i = 1, #ch.list do
    comp = ch.k - ch.list[i]
    
    if temp[comp] then
      return { i - 1, temp[comp] - 1 }
    end
    
    temp[ch.list[i]] = i
  end
end

local cc = CC.new(7)
for _ = 1, 1 do
  local result = { token = cc:solve(get_indizes) }
  local resp, s = cc:submit(result)
  print(resp, s)
end

print(string.format('Average time to find the solution: %fs', cc:runtime_avg()))