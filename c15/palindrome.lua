local CC = require('cc')

local function is_palindrome(challenge)
  local word = challenge.word:gsub('[^%a]', ''):lower()
  local wlen = #word
  local half = math.floor(wlen / 2)
  local first = word:sub(1, wlen % 2 == 0 and half or half + 1)
  local second = word:sub(half + 1, wlen)

  return { token = first == string.reverse(second) }
end

local cc = CC.new(15)
for _ = 1, 10 do
  local resp = cc:solve(is_palindrome, true)
  print(resp)
end

print(string.format('Average time to find the solution: %fs', cc:runtime_avg()))