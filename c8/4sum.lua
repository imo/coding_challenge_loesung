#!/usr/bin/env lua
local CC = require('cc')

-- function which gets the indizes of the summands
local function get_indizes(ch)
  for i1, a in ipairs(ch.list) do
    for i2, b in ipairs(ch.list) do
      for i3, c in ipairs(ch.list) do
        for i4, d in ipairs(ch.list) do
          if ( a + b + c + d ) == ch.k then
            return { i1 - 1, i2 - 1, i3 - 1, i4 - 1 }
          end
        end
      end
    end
  end
end

local cc = CC.new(8)
for _ = 1, 10 do
  local result = { token = cc:solve(get_indizes) }
  local resp = cc:submit(result)
  print(resp)
end

print(string.format('Average time to find the solution: %fs', cc:runtime_avg()))