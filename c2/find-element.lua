#!/usr/bin/env lua

local requests = require('requests')
local time = require('chronos')
local url = 'https://cc.the-morpheus.de/'
local time_tbl = {}

local function get_index(resp_get)
  for i = 1, #resp_get.list do
    if resp_get.list[i] == resp_get.k then
      return i - 1
    end
  end
end

local function get_time(ttbl)
  local t = 0
  for _, v in ipairs(ttbl) do
    t = t + v
  end
  return (t / #ttbl)
end

for i = 1, 100 do
  local resp_get = requests.get(url .. 'challenges/2/sorted/').json()
  local start = time.nanotime()
  local index = get_index(resp_get)
  table.insert(time_tbl, time.nanotime() - start)
  
  local resp_post = requests.post({
      url = url .. 'solutions/2/',
      data = { token = index }
    })
end
os.execute('sleep 2')
print(string.format('Average time to find the solution: %fs', get_time(time_tbl)))
